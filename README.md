## Drones

### Prerequisites
For the next steps Docker, bash, curl, jq and maven should be installed for the build machine

### Build and Start

Run the following maven command to build and start the app:  
`mvn exec:exec@stop-and-remove-docker-containers docker:remove clean install exec:exec@pre-docker-start docker:build docker:start`  
It also runs unit tests on the 'install' step  

### Smoke Tests
There is a script `scripts/smoke_tests.sh` which calls all available API and shows the result

### Notes
Functional requirement about periodic task was skipped