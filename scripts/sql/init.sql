CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

drop table if exists medication;
drop table if exists drone;

create table if not exists drone(
    serial_number varchar primary key,
    model varchar,
    weight_limit int,
    battery_capacity int,
    state varchar
);

create table if not exists medication(
     name varchar,
     code varchar primary key,
     weight int,
     image varchar,
     drone_serial_number varchar,

     constraint drone_medication_fk foreign key (drone_serial_number) references drone(serial_number)
);

insert into drone(serial_number, model, weight_limit, battery_capacity, state)
VALUES (uuid_generate_v4(), 'MIDDLEWEIGHT', 250, 75, 'IDLE'),
       (uuid_generate_v4(), 'CRUISERWEIGHT', 350, 85, 'IDLE'),
       ('static-serial-number', 'CRUISERWEIGHT', 350, 55, 'DELIVERING'),
       (uuid_generate_v4(), 'MIDDLEWEIGHT', 250, 35, 'RETURNING');

insert into medication(name, code, weight, image, drone_serial_number)
VALUES (uuid_generate_v4(), 'MEDICINE1', 100, 'some-image-base64', (select serial_number from drone where state = 'DELIVERING')),
       (uuid_generate_v4(), 'MEDICINE2', 100, 'some-image-base64', (select serial_number from drone where state = 'DELIVERING'));