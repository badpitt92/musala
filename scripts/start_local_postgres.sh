#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
PROJECT_DIR=$(realpath $DIR/..)

docker run \
    -d \
    --name musaladb \
    -p 15432:5432 \
    -e POSTGRES_PASSWORD=postgres \
    -e POSTGRES_USER=musala \
    -e POSTGRES_DB=musala \
    timms/postgres-logging:9.6

# just wait for postgres
# adjust the value for your env
sleep 10

docker cp ${PROJECT_DIR}/scripts/sql/init.sql musaladb:/init.sql

docker exec musaladb psql -U musala -f init.sql