#!/bin/bash

stopAndRemoveAllContainers() {
  if [[ $(docker ps -aq) ]]; then
	  docker rm -f $(docker ps -aq)
	fi
}

stopAndRemoveAllContainers