#!/usr/bin/env bash

-e

makeGetCurlRequest() {
  url="$1"
  filename=tmp.json
  stdout=$(curl --request GET -sL \
       -w "%{http_code}" \
       --url $url \
       -o $filename)
  curl_status=$?
  if [[ $curl_status != 0 ]]; then
    die "Curl error code $curl_status. Stdout: $stdout"
  fi
  cat $filename | jq
  rm $filename
}

echo "----------Query--for--all--available--drones------------------------------"

makeGetCurlRequest http://localhost:8080/api/drones

echo -e "\n----------Query--for--battery--level--of--one-drone-----------------------"

makeGetCurlRequest http://localhost:8080/api/drones/static-serial-number/battery

echo -e "\n----------Query--for--load--of--one-drone---------------------------------"

makeGetCurlRequest http://localhost:8080/api/drones/static-serial-number/load

echo -e "\n----------Register--new--drone--------------------------------------------"

serialNumber=$(cat /proc/sys/kernel/random/uuid)
curl -d '{"serialNumber":"'$serialNumber'","model":"LIGHTWEIGHT","weightLimit":50,"batteryCapacity":99}' \
     -sL \
     -H "Content-Type: application/json" \
     --url 'http://localhost:8080/api/drones'

echo -e "\n----------Query--for--all--available--drones------------------------------"

makeGetCurlRequest http://localhost:8080/api/drones

echo -e "\n----------Add--load--to--drone--------------------------------------------"

name=$(cat /proc/sys/kernel/random/uuid)
code=$(cat /proc/sys/kernel/random/uuid)
code=$(echo "${code^^}" | sed -E 's/-/_/g')
echo "$code"
curl -d '{"load":[{"name":"'$name'","code":"'$code'","weight":10,"image":"image1"}]}' \
     -sL \
     -H "Content-Type: application/json" \
     --url 'http://localhost:8080/api/drones/'$serialNumber'/load'

echo -e "\n----------Query--for--load--of--one-drone---------------------------------"

makeGetCurlRequest http://localhost:8080/api/drones/$serialNumber/load

echo -e "\n--------------------------------------------------------------------------"
