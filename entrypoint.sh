
exec java \
  -server \
  -Xmx1024M \
  -XX:+ExitOnOutOfMemoryError \
  -XX:+HeapDumpOnOutOfMemoryError \
  -XX:HeapDumpPath="/logs/admin-portal-heap-$(date '+%Y-%m-%d-%H-%M-%S').hprof" \
  -XX:+UseG1GC \
  -XX:MaxGCPauseMillis=100 \
  -Dfile.encoding=UTF-8 \
  -Dlog4j2.formatMsgNoLookups=true \
  ${DEBUGGING_AGENT} \
  -cp /drones.jar \
  org.springframework.boot.loader.PropertiesLauncher