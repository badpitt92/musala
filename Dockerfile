FROM adoptopenjdk/openjdk11:jre-11.0.3_7-alpine

ENV JAR_FILE="drones.jar"

COPY $JAR_FILE /$JAR_FILE
COPY entrypoint.sh /
ENTRYPOINT ["sh", "entrypoint.sh"]