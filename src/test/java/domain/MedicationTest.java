package domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import musala.domain.ValidationException;
import musala.domain.drone.Drone;
import musala.domain.drone.DroneEventListener;
import musala.domain.drone.DroneState;
import musala.domain.load.Medication;
import musala.domain.load.MedicationEventListener;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

public class MedicationTest {

    private static final String name = "UniqueName12";
    private static final String code = "UNIQUE_CODE";
    private static final int weight = 75;
    private static final String image = "base64image";
    private static final MedicationEventListener listener = Mockito.mock(MedicationEventListener.class);

    @Test
    public void createMedication_WithValidation_Successful() {
        MedicationEventListener mockListener = Mockito.mock(MedicationEventListener.class);
        Medication medication = Medication.buildNewMedication()
                .name(name)
                .code(code)
                .weight(weight)
                .image(image)
                .listener(mockListener)
                .build();

        assertNotNull(medication);
        assertEquals(name, medication.getName());
        assertEquals(code, medication.getCode());
        assertEquals(weight, medication.getWeight());
        assertEquals(image, medication.getImage());
        assertEquals(mockListener, medication.getListener());
        verify(mockListener).created(eq(medication));
    }

    public static Stream<Arguments> provideInputData() {
        return Stream.of(
                Arguments.of(
                    new MedicationData(
                            "awd$#",
                            code,
                            weight,
                            image,
                            listener
                    ),
                    "Name"
                ),
                Arguments.of(
                        new MedicationData(
                                name,
                                code + "adw",
                                weight,
                                image,
                                listener
                        ),
                        "Code"
                ),
                Arguments.of(
                        new MedicationData(
                                name,
                                code,
                                0,
                                image,
                                listener
                        ),
                        "Weight"
                ),
                Arguments.of(
                        new MedicationData(
                                name,
                                code,
                                weight,
                                null,
                                listener
                        ),
                        "Image_Null"
                ),
                Arguments.of(
                        new MedicationData(
                                name,
                                code,
                                weight,
                                image,
                                null
                        ),
                        "Listener_Null"
                )
        );
    }

    @Getter
    @AllArgsConstructor
    public static class MedicationData {
        private String name;
        private String code;
        private int weight;
        private String image;
        private MedicationEventListener listener;
    }

    @ParameterizedTest(name = "{1}")
    @MethodSource("provideInputData")
    public void createDrone_WithValidation_Failed(MedicationData medicationData,
                                                  String methodName) {
        assertThrows(
                ValidationException.class,
                () -> Medication.buildNewMedication()
                        .name(medicationData.getName())
                        .code(medicationData.getCode())
                        .weight(medicationData.getWeight())
                        .image(medicationData.getImage())
                        .listener(medicationData.getListener())
                        .build()
        );
    }
}
