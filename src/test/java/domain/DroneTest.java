package domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import musala.domain.*;
import musala.domain.drone.Drone;
import musala.domain.drone.DroneEventListener;
import musala.domain.drone.DroneState;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

public class DroneTest {

    private static final String serialNumber = "TestSerialNumber";
    private static final String model = "LIGHTWEIGHT";
    private static final DroneState state = DroneState.IDLE;
    private static final int batteryCapacity = 75;
    private static final int weightLimit = 250;
    private static final DroneEventListener listener = Mockito.mock(DroneEventListener.class);

    @Test
    public void createDrone_WithValidation_Successful() {
        DroneEventListener mockListener = Mockito.mock(DroneEventListener.class);
        Drone drone = Drone.buildNewDrone()
                .serialNumber(serialNumber)
                .batteryCapacity(batteryCapacity)
                .model(model)
                .state(state)
                .weightLimit(weightLimit)
                .listener(mockListener)
                .build();

        assertNotNull(drone);
        assertEquals(serialNumber, drone.getSerialNumber());
        assertEquals(model, drone.getModel().name());
        assertEquals(state, drone.getState());
        assertEquals(batteryCapacity, drone.getBatteryCapacity());
        assertEquals(weightLimit, drone.getWeightLimit());
        assertEquals(mockListener, drone.getListener());
        verify(mockListener).created(eq(drone));
    }

    public static Stream<Arguments> provideInputData() {
        return Stream.of(
                Arguments.of(
                    new DroneData(
                            Stream.generate(() -> ".")
                                    .limit(101)
                                    .collect(Collectors.joining()),
                            model,
                            state,
                            batteryCapacity,
                            weightLimit,
                            listener
                    ),
                    "SerialNumber"
                ),
                Arguments.of(
                        new DroneData(
                                serialNumber,
                                "NEW-MODEL",
                                state,
                                batteryCapacity,
                                weightLimit,
                                listener
                        ),
                        "Model"
                ),
                Arguments.of(
                        new DroneData(
                                serialNumber,
                                model,
                                null,
                                batteryCapacity,
                                weightLimit,
                                listener
                        ),
                        "State"
                ),
                Arguments.of(
                        new DroneData(
                                serialNumber,
                                model,
                                state,
                                0,
                                weightLimit,
                                listener
                        ),
                        "Capacity_Zero"
                ),
                Arguments.of(
                        new DroneData(
                                serialNumber,
                                model,
                                state,
                                101,
                                weightLimit,
                                listener
                        ),
                        "Capacity_Above100"
                ),
                Arguments.of(
                        new DroneData(
                                serialNumber,
                                model,
                                state,
                                batteryCapacity,
                                0,
                                listener
                        ),
                        "WeightLimit_Zero"
                ),
                Arguments.of(
                        new DroneData(
                                serialNumber,
                                model,
                                state,
                                batteryCapacity,
                                Drone.MAX_WEIGHT_LIMIT + 1,
                                listener
                        ),
                        "WeightLimit_AboveMaxValue"
                ),
                Arguments.of(
                        new DroneData(
                                serialNumber,
                                model,
                                state,
                                batteryCapacity,
                                weightLimit,
                                null
                        ),
                        "Listener_Null"
                )
        );
    }

    @Getter
    @AllArgsConstructor
    public static class DroneData {
        private String serialNumber;
        private String model;
        private DroneState state;
        private int batteryCapacity;
        private int weightLimit;
        private DroneEventListener listener;
    }

    @ParameterizedTest(name = "{1}")
    @MethodSource("provideInputData")
    public void createDrone_WithValidation_Failed(DroneData droneData,
                                                  String methodName) {
        assertThrows(
                ValidationException.class,
                () -> Drone.buildNewDrone()
                        .serialNumber(droneData.getSerialNumber())
                        .batteryCapacity(droneData.getBatteryCapacity())
                        .model(droneData.getModel())
                        .state(droneData.getState())
                        .weightLimit(droneData.getWeightLimit())
                        .listener(droneData.getListener())
                        .build()
        );
    }
}
