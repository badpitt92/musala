package musala.domain.queries;

import lombok.AllArgsConstructor;
import musala.domain.drone.Drone;
import musala.domain.drone.DroneRepository;
import musala.domain.drone.DroneState;

import java.util.List;

@AllArgsConstructor
public class DronesForLoadingQueryImpl implements DronesForLoadingQuery {
    private final DroneRepository droneRepository;
    @Override
    public List<Drone> findAvailableForLoadingDrones(Filter filter) {
        return droneRepository.getByStateAndBatteryLevel(
                // can be retrieved from filter
                DroneState.IDLE, 25
        );
    }
}
