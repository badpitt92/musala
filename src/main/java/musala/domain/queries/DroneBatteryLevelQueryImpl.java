package musala.domain.queries;

import lombok.AllArgsConstructor;
import musala.domain.drone.DroneRepository;

@AllArgsConstructor
public class DroneBatteryLevelQueryImpl implements DroneBatteryLevelQuery {
    private final DroneRepository droneRepository;

    public int findBatteryCapacityBySerialNumber(String serialNumber) {
        return droneRepository.getBatteryCapacityBySerialNumber(serialNumber);
    }
}
