package musala.domain.queries;

import musala.domain.load.Medication;

import java.util.List;

public interface DroneLoadQuery {
    List<Medication> findLoadByDroneSerialNumber(String serialNumber);
}
