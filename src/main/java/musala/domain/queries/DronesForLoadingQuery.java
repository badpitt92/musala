package musala.domain.queries;

import musala.domain.drone.Drone;

import java.util.List;

public interface DronesForLoadingQuery {
    List<Drone> findAvailableForLoadingDrones(Filter filter);
    class Filter {

    }
}
