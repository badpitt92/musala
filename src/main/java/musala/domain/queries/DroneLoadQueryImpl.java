package musala.domain.queries;

import lombok.AllArgsConstructor;
import musala.domain.load.Medication;
import musala.domain.load.MedicationRepository;

import java.util.List;

@AllArgsConstructor
public class DroneLoadQueryImpl implements DroneLoadQuery {
    private final MedicationRepository medicationRepository;
    @Override
    public List<Medication> findLoadByDroneSerialNumber(String serialNumber) {
        return medicationRepository.getMedicationsByDroneSerialNumber(serialNumber);
    }
}
