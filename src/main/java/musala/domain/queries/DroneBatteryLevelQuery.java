package musala.domain.queries;

public interface DroneBatteryLevelQuery {
    int findBatteryCapacityBySerialNumber(String serialNumber);
}
