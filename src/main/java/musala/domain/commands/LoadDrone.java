package musala.domain.commands;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

public interface LoadDrone extends Command<LoadDrone.LoadDroneModel> {

    @Getter
    @AllArgsConstructor
    class LoadDroneModel {
        private String serialNumber;
        private List<LoadModel> load;
    }

    @Getter
    @AllArgsConstructor
    class LoadModel {
        private String name;
        private int weight;
        private String code;
        private String image;
    }
}
