package musala.domain.commands;

public interface Command<DATA> {
    void execute(DATA data);
}
