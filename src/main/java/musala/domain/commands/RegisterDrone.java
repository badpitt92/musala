package musala.domain.commands;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

public interface RegisterDrone extends Command<RegisterDrone.RegisterDroneModel> {

    @Builder
    @Getter
    @AllArgsConstructor
    class RegisterDroneModel {
        private String serialNumber;
        private String model;
        private int batteryCapacity;
        private int weightLimit;
    }
}
