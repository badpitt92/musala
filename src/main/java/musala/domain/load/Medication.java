package musala.domain.load;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import musala.domain.ValidationException;
import org.apache.commons.lang3.StringUtils;

import static musala.domain.Utils.validateAboveZero;
import static musala.domain.Utils.validateNotNull;

@Getter
public class Medication extends Load {
    private final String name; // only letters, numbers, '-' and '_'
    private final String image; // limit on size? or URL?

    private final MedicationEventListener listener;

    public Medication(String name,
                      String code,
                      int weight,
                      String image,
                      MedicationEventListener listener) {
        super(weight, code);
        this.name = name;
        this.image = image;
        this.listener = listener;
    }

    @Builder(builderMethodName = "buildNewMedication")
    public static Medication createNewMedication(String name,
                                                 String code,
                                                 int weight,
                                                 String image,
                                                 MedicationEventListener listener) {
        validateMedicationName(name);
        validateMedicationCode(code);
        validateAboveZero(weight, "Medication weight");
        validateNotNull(listener, "Listener");
        validateNotNull(image, "medication image");


        Medication medication = new Medication(
                name,
                code,
                weight,
                image,
                listener
        );
        listener.created(medication);
        return medication;
    }

    private static final String LOWERCASE_CHARACTERS = "abcdefghijklmnopqrstuvwxyz";
    private static final String UPPERCASE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String NUMBER_CHARACTERS = "0123456789";
    private static final String VALID_NAME_CHARACTERS =
            LOWERCASE_CHARACTERS +
            UPPERCASE_CHARACTERS +
            NUMBER_CHARACTERS +
            "-_";
    private static final String VALID_CODE_CHARACTERS =
            UPPERCASE_CHARACTERS +
            NUMBER_CHARACTERS +
            "_";
    private static void validateMedicationName(String name) {
        validateNotNull(name, "Medication name");
        if (!StringUtils.containsOnly(name, VALID_NAME_CHARACTERS)) {
            throw new ValidationException(
                    String.format(
                            "The medication name should contain only valid characters ('%s')",
                            VALID_NAME_CHARACTERS
                    )
            );
        }
    }
    private static void validateMedicationCode(String name) {
        validateNotNull(name, "Medication code");
        if (!StringUtils.containsOnly(name, VALID_CODE_CHARACTERS)) {
            throw new ValidationException(
                    String.format(
                            "The medication code should contain only valid characters ('%s')",
                            VALID_CODE_CHARACTERS
                    )
            );
        }
    }
}
