package musala.domain.load;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public abstract class Load {
    private int weight;
    private String code;
}
