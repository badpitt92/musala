package musala.domain.load;

import java.util.List;

public interface MedicationRepository {
    List<Medication> getMedicationsByDroneSerialNumber(String serialNumber);
}
