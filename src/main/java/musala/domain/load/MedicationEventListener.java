package musala.domain.load;

public interface MedicationEventListener {
    void created(Medication medication);
}
