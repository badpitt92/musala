package musala.domain;

public final class Utils {

    private Utils() {}

    public static void validateNotNull(Object target, String name) {
        if (target == null) {
            throw new ValidationException(
                    String.format(
                            "%s must be set",
                            name
                    )
            );
        }
    }
    public static void validateAboveZero(int target, String name) {
        if (target <= 0) {
            throw new ValidationException(
                    String.format(
                            "%s have to be grater then zero",
                            name
                    )
            );
        }
    }
}
