package musala.domain.drone;

import musala.domain.load.Load;

import java.util.List;

public interface DroneEventListener {
    void created(Drone drone);
    void loadAdded(Drone drone, List<? extends Load> load);
    void stateChanged(Drone drone, DroneState state);
}
