package musala.domain.drone;

import java.util.List;

public interface DroneRepository {
    Drone getBySerialNumber(String serialNumber);
    Integer getBatteryCapacityBySerialNumber(String serialNumber);
    List<Drone> getByStateAndBatteryLevel(DroneState state, int minimalBatteryLevel);
}
