package musala.domain.drone;

import lombok.Builder;
import lombok.Getter;
import musala.domain.load.Load;
import musala.domain.ValidationException;

import java.util.ArrayList;
import java.util.List;

import static musala.domain.Utils.validateAboveZero;
import static musala.domain.Utils.validateNotNull;

@Getter
public class Drone {
    public static final int MAX_WEIGHT_LIMIT = 500;

    private final String serialNumber; // max - 100 characters
    private final Model model;
    private final int weightLimit; // max - MAX_WEIGHT_LIMIT
    private final int batteryCapacity; // 0 - 100
    private final DroneState state;

    private final List<Load> load;

    private final DroneEventListener listener;

    public Drone(String serialNumber, Model model,
                 int weightLimit, int batteryCapacity,
                 DroneState state,
                 DroneEventListener listener) {
        this.serialNumber = serialNumber;
        this.model = model;
        this.weightLimit = weightLimit;
        this.batteryCapacity = batteryCapacity;
        this.state = state;
        this.listener = listener;
        this.load = new ArrayList<>();
    }

    public void addLoad(List<? extends Load> load) {
        Integer currentWeight = this.load.stream()
                .map(Load::getWeight)
                .reduce(Integer::sum)
                .orElse(0);
        if (currentWeight >= MAX_WEIGHT_LIMIT) {
            throw new ValidationException("Drone is carrying more than it's limit allows");
        }
        Integer addingWeight = load.stream()
                .map(Load::getWeight)
                .reduce(Integer::sum)
                .orElse(0);
        if (currentWeight + addingWeight >= MAX_WEIGHT_LIMIT) {
            throw new ValidationException("Drone can't carry more than it's limit allows");
        }
        if (this.batteryCapacity < 25) {
            throw new ValidationException(
                    String.format(
                            "Battery level is too low ('%s') for new load",
                            this.batteryCapacity
                    )
            );
        }
        this.load.addAll(load);
        this.listener.loadAdded(this, load);
        this.listener.stateChanged(this, DroneState.LOADED);
    }

    @Builder(builderMethodName = "buildNewDrone")
    public static Drone createNewDrone(String serialNumber,
                                       String model,
                                       int weightLimit,
                                       int batteryCapacity,
                                       DroneState state,
                                       DroneEventListener listener) {
        validateSerialNumber(serialNumber);
        validateBatteryCapacity(batteryCapacity);
        validateWeightLimit(weightLimit);
        validateNotNull(listener, "Listener");
        validateNotNull(model, "Model");
        validateNotNull(state, "State");


        Drone drone = new Drone(
                serialNumber,
                getModel(model),
                weightLimit,
                batteryCapacity,
                state,
                listener
        );
        listener.created(drone);
        return drone;
    }
    private static void validateSerialNumber(String serialNumber) {
        validateNotNull(serialNumber, "Serial number");
        if (serialNumber.length() > 100) {
            throw new ValidationException("The maximum value is 100 characters");
        }
    }
    private static void validateBatteryCapacity(int batteryCapacity) {
        validateAboveZero(batteryCapacity, "Battery capacity");
        if (batteryCapacity > 100) {
            throw new ValidationException("The maximum value for Battery capacity is 100");
        }
    }
    private static void validateWeightLimit(int weightLimit) {
        validateAboveZero(weightLimit, "Weight limit");
        if (weightLimit > MAX_WEIGHT_LIMIT) {
            throw new ValidationException(
                    String.format(
                        "The maximum value for Weight limit is '%s'",
                        MAX_WEIGHT_LIMIT
                    )
            );
        }
    }
    private static Model getModel(String name) {
        for (Model value : Model.values()) {
            if (value.name().equals(name)) {
                return value;
            }
        }
        throw new ValidationException(
                String.format("Model '%s' is not found", name)
        );
    }
}