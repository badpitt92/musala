package musala.infrastructure.repository.repositories;

import lombok.AllArgsConstructor;
import musala.domain.load.Medication;
import musala.domain.load.MedicationEventListener;
import musala.domain.load.MedicationRepository;
import musala.infrastructure.repository.entity.MedicationEntity;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
public class MedicationDatabaseRepository implements MedicationRepository {
    private final MedicationSpringDataRepository medicationSpringDataRepository;
    private final MedicationEventListener medicationEventListener;
    @Override
    public List<Medication> getMedicationsByDroneSerialNumber(String serialNumber) {
        return medicationSpringDataRepository
                .findDronesMedicationModelsBySerialNumber(serialNumber)
                .stream()
                .map(this::mapToDomain)
                .collect(Collectors.toList());
    }

    private Medication mapToDomain(MedicationEntity entity) {
        return new Medication(
                entity.getName(),
                entity.getCode(),
                entity.getWeight(),
                entity.getImage(),
                medicationEventListener
        );
    }
}
