package musala.infrastructure.repository.repositories;

import musala.infrastructure.repository.entity.MedicationEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MedicationSpringDataRepository extends Repository<MedicationEntity, String> {

    @Query(
            value = "SELECT " +
                    " e " +
                    "FROM MedicationEntity e WHERE " +
                    "  e.drone.serialNumber = :drone"
    )
    List<MedicationEntity> findDronesMedicationModelsBySerialNumber(@Param("drone") String drone);
}
