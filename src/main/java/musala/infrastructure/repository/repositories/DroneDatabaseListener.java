package musala.infrastructure.repository.repositories;

import musala.domain.drone.Drone;
import musala.domain.drone.DroneEventListener;
import musala.domain.drone.DroneState;
import musala.domain.load.Load;
import musala.infrastructure.repository.entity.DroneEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public interface DroneDatabaseListener extends DroneEventListener, Repository<DroneEntity, UUID> {

    @Modifying
    @Query(
            value = "INSERT INTO drone(serial_number, model, weight_limit, battery_capacity, state) values"
                    + " (:serialNumber, :model, :weightLimit, :batteryCapacity, :state)",
            nativeQuery = true
    )
    void create(@Param("serialNumber") String serialNumber,
                @Param("model") String model,
                @Param("weightLimit") int weightLimit,
                @Param("batteryCapacity") int batteryCapacity,
                @Param("state") String state);

    @Modifying
    @Query(
            value = "update drone " +
                    "set state = :state " +
                    "where serial_number = :serialNumber",
            nativeQuery = true
    )
    void updateState(@Param("serialNumber") String serialNumber,
                     @Param("state") String state);

    @Modifying
    @Query(
            value = "update medication " +
                    "set drone_serial_number = :drone " +
                    "where code in :codes",
            nativeQuery = true
    )
    void addMedicineToDrone(@Param("drone") String serialNumber,
                            @Param("codes") List<String> codes);

    @Override
    default void created(Drone drone) {
        create(
                drone.getSerialNumber(),
                drone.getModel().name(),
                drone.getWeightLimit(),
                drone.getBatteryCapacity(),
                drone.getState().name()
        );
    }

    @Override
    default void loadAdded(Drone drone, List<? extends Load> load) {
        addMedicineToDrone(
                drone.getSerialNumber(),
                load.stream()
                        .map(Load::getCode)
                        .collect(Collectors.toList())
        );
    }

    @Override
    default void stateChanged(Drone drone, DroneState state) {
        updateState(drone.getSerialNumber(), state.name());
    }
}
