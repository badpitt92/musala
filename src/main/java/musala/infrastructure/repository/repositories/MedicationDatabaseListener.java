package musala.infrastructure.repository.repositories;

import musala.domain.load.Medication;
import musala.domain.load.MedicationEventListener;
import musala.infrastructure.repository.entity.MedicationEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

public interface MedicationDatabaseListener extends MedicationEventListener, Repository<MedicationEntity, String> {
    @Modifying
    @Query(
            value = "INSERT INTO medication(name, code, weight, image) values"
                    + " (:name, :code, :weight, :image)",
            nativeQuery = true
    )
    void create(@Param("name") String name,
                @Param("code") String code,
                @Param("weight") int weight,
                @Param("image") String image);

    @Override
    default void created(Medication medication) {
        create(
                medication.getName(),
                medication.getCode(),
                medication.getWeight(),
                medication.getImage()
        );
    }
}
