package musala.infrastructure.repository.repositories;

import musala.infrastructure.repository.entity.DroneEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;

public interface DroneSpringDataRepository extends Repository<DroneEntity, String> {

    @Query(
            value = "SELECT " +
                    "  batteryCapacity as batteryCapacity " +
                    "FROM DroneEntity WHERE " +
                    "  serialNumber = :serialNumber"
    )
    Integer findBatteryCapacityBySerialNumber(@Param("serialNumber") String serialNumber);

    @Query(
            value = "SELECT " +
                    " e " +
                    "FROM DroneEntity e WHERE " +
                    "  e.serialNumber = :serialNumber"
    )
    DroneEntity findDroneModelBySerialNumber(@Param("serialNumber") String serialNumber);

    @Query(
            value = "SELECT " +
                    " e " +
                    "FROM DroneEntity e WHERE " +
                    "  e.state = :state " +
                    "  and e.batteryCapacity >= :batteryCapacity"
    )
    Collection<DroneEntity> findAvailableDronesModelsByStateAndBatteryCapacity(@Param("state") String state,
                                                                               @Param("batteryCapacity") int batteryCapacity);
}
