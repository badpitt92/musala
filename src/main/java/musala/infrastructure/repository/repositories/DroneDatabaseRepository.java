package musala.infrastructure.repository.repositories;

import lombok.AllArgsConstructor;
import musala.domain.drone.*;
import musala.infrastructure.repository.entity.DroneEntity;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
public class DroneDatabaseRepository implements DroneRepository {
    private final DroneSpringDataRepository springDataRepository;
    private final DroneEventListener droneEventListener;
    @Override
    public Drone getBySerialNumber(String serialNumber) {
        return this.mapToDomain(
                springDataRepository.findDroneModelBySerialNumber(serialNumber)
        );
    }

    @Override
    public Integer getBatteryCapacityBySerialNumber(String serialNumber) {
        return springDataRepository.findBatteryCapacityBySerialNumber(serialNumber);
    }

    @Override
    public List<Drone> getByStateAndBatteryLevel(DroneState state, int minimalBatteryLevel) {
        return springDataRepository
                .findAvailableDronesModelsByStateAndBatteryCapacity(state.name(), minimalBatteryLevel)
                .stream()
                .map(this::mapToDomain)
                .collect(Collectors.toList());
    }

    private Drone mapToDomain(DroneEntity entity) {
        return new Drone(
                entity.getSerialNumber(),
                Model.valueOf(entity.getModel()),
                entity.getWeightLimit(),
                entity.getBatteryCapacity(),
                DroneState.valueOf(entity.getState()),
                droneEventListener
        );
    }
}
