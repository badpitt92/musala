package musala.infrastructure.repository.entity;

import lombok.*;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "drone")
@EqualsAndHashCode(of = "serialNumber")
public class DroneEntity {
    @Id
    @Column(name = "serial_number")
    private String serialNumber;
    private String model;
    @Column(name = "weight_limit")
    private int weightLimit;
    @Column(name = "battery_capacity")
    private int batteryCapacity;
    private String state;
    @OneToMany(mappedBy = "drone", cascade = {CascadeType.ALL}, orphanRemoval = true /* assumption: we don't care about delivered medication */)
    private List<MedicationEntity> medicationEntities = new LinkedList<>();
}
