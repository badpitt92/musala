package musala.infrastructure.repository.entity;

import lombok.*;

import javax.persistence.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "medication")
@EqualsAndHashCode(of = "code")
public class MedicationEntity {
    private String name;
    @Id
    private String code;
    private int weight;
    private String image;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
            name = "drone_serial_number"
    )
    private DroneEntity drone;
}
