package musala.infrastructure;

import lombok.AllArgsConstructor;
import musala.domain.drone.Drone;
import musala.domain.commands.LoadDrone;
import musala.domain.drone.DroneRepository;
import musala.domain.load.Medication;
import musala.domain.load.MedicationEventListener;

import java.util.stream.Collectors;

@AllArgsConstructor
public class LoadDroneImpl implements LoadDrone {
    private final DroneRepository droneRepository;
    private final MedicationEventListener medicationEventListener;

    @Override
    public void execute(LoadDrone.LoadDroneModel loadDroneModel) {
        Drone drone = droneRepository.getBySerialNumber(loadDroneModel.getSerialNumber());
        drone.addLoad(
                loadDroneModel.getLoad().stream()
                        .map(loadModel -> Medication.buildNewMedication()
                                .name(loadModel.getName())
                                .code(loadModel.getCode())
                                .weight(loadModel.getWeight())
                                .image(loadModel.getImage())
                                .listener(medicationEventListener)
                                .build()
                        )
                        .collect(Collectors.toList())
        );
    }
}
