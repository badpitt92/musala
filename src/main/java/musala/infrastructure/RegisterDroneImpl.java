package musala.infrastructure;

import lombok.AllArgsConstructor;
import musala.domain.drone.Drone;
import musala.domain.drone.DroneEventListener;
import musala.domain.drone.DroneState;
import musala.domain.commands.RegisterDrone;

@AllArgsConstructor
public class RegisterDroneImpl implements RegisterDrone {

    private DroneEventListener listener;

    @Override
    public void execute(RegisterDroneModel data) {
        Drone.buildNewDrone()
                .serialNumber(data.getSerialNumber())
                .batteryCapacity(data.getBatteryCapacity())
                .state(DroneState.IDLE)
                .model(data.getModel())
                .weightLimit(data.getWeightLimit())
                .listener(this.listener)
                .build();
    }
}
