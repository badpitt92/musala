package musala.infrastructure;

import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class RepositoryTransactionDecorator<T, R> implements Function<T, R> {

    private final Function<T, R> nextFunction;
    private final TransactionTemplate template;

    public RepositoryTransactionDecorator(
            Function<T, R> nextFunction,
            PlatformTransactionManager transactionManager,
            Consumer<TransactionTemplate> mutateTemplate
    ) {
        this.nextFunction = nextFunction;
        this.template = new TransactionTemplate(transactionManager);
        mutateTemplate.accept(this.template);
    }

    public RepositoryTransactionDecorator(
            Consumer<T> nextConsumer,
            PlatformTransactionManager transactionManager,
            Consumer<TransactionTemplate> mutateTemplate
    ) {
        this.nextFunction = t -> {
            nextConsumer.accept(t);
            return null;
        };
        this.template = new TransactionTemplate(transactionManager);
        mutateTemplate.accept(this.template);
    }

    public RepositoryTransactionDecorator(
            Supplier<R> nextSupplier,
            PlatformTransactionManager transactionManager,
            Consumer<TransactionTemplate> mutateTemplate
    ) {
        this.nextFunction = t -> nextSupplier.get();
        this.template = new TransactionTemplate(transactionManager);
        mutateTemplate.accept(this.template);
    }

    public static <T, R> Function<T, R> decorateTransaction(
            Function<T, R> nextFunction,
            PlatformTransactionManager transactionManager,
            Consumer<TransactionTemplate> mutateTemplate
    ) {
        return new RepositoryTransactionDecorator<>(nextFunction, transactionManager, mutateTemplate);
    }

    public static <T, R> Function<T, R> decorateTransaction(
            Consumer<T> nextConsumer,
            PlatformTransactionManager transactionManager,
            Consumer<TransactionTemplate> mutateTemplate
    ) {
        return new RepositoryTransactionDecorator<>(nextConsumer, transactionManager, mutateTemplate);
    }

    public static <T, R> Function<T, R> decorateTransaction(
            Supplier<R> nextSupplier,
            PlatformTransactionManager transactionManager,
            Consumer<TransactionTemplate> mutateTemplate
    ) {
        return new RepositoryTransactionDecorator<>(nextSupplier, transactionManager, mutateTemplate);
    }

    @Override
    public R apply(T t) {
        return template.execute(status -> nextFunction.apply(t));
    }
}
