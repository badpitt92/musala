package musala.infrastructure.configuration;

import musala.domain.commands.LoadDrone;
import musala.domain.commands.RegisterDrone;
import musala.domain.drone.DroneEventListener;
import musala.domain.drone.DroneRepository;
import musala.domain.load.MedicationEventListener;
import musala.domain.load.MedicationRepository;
import musala.domain.queries.*;
import musala.infrastructure.LoadDroneImpl;
import musala.infrastructure.RegisterDroneImpl;
import musala.infrastructure.web.DroneController;
import musala.infrastructure.web.DroneControllerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;

import static musala.infrastructure.RepositoryTransactionDecorator.decorateTransaction;

@Configuration
@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
public class DroneConfiguration {

    @Autowired
    private PlatformTransactionManager transactionManager;

    @Bean
    public RegisterDrone registerDroneCommand(DroneEventListener droneEventListener) {
        RegisterDrone registerDrone = new RegisterDroneImpl(droneEventListener);
        return decorateTransaction(
                registerDrone::execute,
                transactionManager,
                template -> {
                    template.setName("register-new-drone");
                    template.setReadOnly(false);
                }
        )::apply;
    }

    @Bean
    public LoadDrone loadDroneCommand(DroneRepository droneRepository,
                                      MedicationEventListener medicationEventListener) {
        LoadDrone loadDrone = new LoadDroneImpl(droneRepository, medicationEventListener);
        return decorateTransaction(
                loadDrone::execute,
                transactionManager,
                template -> {
                    template.setName("add-load-to-drone");
                    template.setReadOnly(false);
                }
        )::apply;
    }

    @Bean
    public DroneBatteryLevelQuery droneBatteryLevelQuery(DroneRepository droneRepository) {
        DroneBatteryLevelQuery query = new DroneBatteryLevelQueryImpl(droneRepository);
        return decorateTransaction(
                query::findBatteryCapacityBySerialNumber,
                transactionManager,
                template -> {
                    template.setName("get-battery-capacity-for-drone");
                    template.setReadOnly(true);
                }
        )::apply;
    }

    @Bean
    public DronesForLoadingQuery dronesForLoadingQuery(DroneRepository droneRepository) {
        DronesForLoadingQuery query = new DronesForLoadingQueryImpl(droneRepository);
        return decorateTransaction(
                query::findAvailableForLoadingDrones,
                transactionManager,
                template -> {
                    template.setName("get-battery-capacity-for-drone");
                    template.setReadOnly(true);
                }
        )::apply;
    }

    @Bean
    public DroneLoadQuery droneLoadQuery(MedicationRepository medicationRepository) {
        DroneLoadQuery query = new DroneLoadQueryImpl(medicationRepository);
        return decorateTransaction(
                query::findLoadByDroneSerialNumber,
                transactionManager,
                template -> {
                    template.setName("get-battery-capacity-for-drone");
                    template.setReadOnly(true);
                }
        )::apply;
    }

    @Bean
    public DroneController droneController(RegisterDrone registerDrone,
                                           LoadDrone loadDrone,
                                           DroneBatteryLevelQuery droneBatteryLevelQuery,
                                           DronesForLoadingQuery dronesForLoadingQuery,
                                           DroneLoadQuery droneLoadQuery) {
        return new DroneControllerImpl(
                registerDrone,
                loadDrone,
                droneBatteryLevelQuery,
                dronesForLoadingQuery,
                droneLoadQuery
        );
    }
}
