package musala.infrastructure.configuration.database;

import musala.domain.drone.DroneEventListener;
import musala.domain.drone.DroneRepository;
import musala.domain.load.MedicationEventListener;
import musala.domain.load.MedicationRepository;
import musala.infrastructure.repository.repositories.DroneDatabaseRepository;
import musala.infrastructure.repository.repositories.DroneSpringDataRepository;
import musala.infrastructure.repository.repositories.MedicationDatabaseRepository;
import musala.infrastructure.repository.repositories.MedicationSpringDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.persistence.EntityManager;

@Configuration
@EnableJpaRepositories("musala.infrastructure.repository.repositories")
@EntityScan("musala.infrastructure.repository.entity")
@EnableAutoConfiguration
@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
public class DatabaseConfiguration {

    // ------ Drone -------

    @Autowired
    DroneSpringDataRepository droneSpringDataRepository;

    @Autowired
    DroneEventListener droneDatabaseListener;

    @Bean
    public DroneRepository droneDatabaseRepository() {
        return new DroneDatabaseRepository(droneSpringDataRepository, droneDatabaseListener);
    }

    // ------ Medication -------

    @Autowired
    MedicationSpringDataRepository medicationSpringDataRepository;

    @Autowired
    MedicationEventListener medicationDatabaseListener;

    @Bean
    public MedicationRepository medicationRepository() {
        return new MedicationDatabaseRepository(medicationSpringDataRepository, medicationDatabaseListener);
    }

    @Autowired
    EntityManager entityManager;
}

