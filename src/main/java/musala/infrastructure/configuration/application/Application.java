package musala.infrastructure.configuration.application;

import musala.infrastructure.configuration.DroneConfiguration;
import musala.infrastructure.configuration.database.DatabaseConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({
        DatabaseConfiguration.class,
        DroneConfiguration.class
})
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
