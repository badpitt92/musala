package musala.infrastructure.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("api")
public interface DroneController {
    @PostMapping(value = "drones", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    void registerDrone(
            @RequestBody RegisterDroneRequest registerRequest
    );

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    class RegisterDroneRequest {
        private String serialNumber;
        private String model;
        private int weightLimit;
        private int batteryCapacity;
        private String state;
    }

    @PostMapping(value = "drones/{serialNumber}/load", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    void addLoadToDrone(
            @PathVariable String serialNumber,
            @RequestBody AddLoadToDroneRequest request
    );

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    class AddLoadToDroneRequest {
        private List<LoadDetails> load;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    class LoadDetails {
        private String name;
        private String code;
        private int weight;
        private String image;
    }

    @GetMapping(value = "drones", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    Collection<AvailableDroneModel> getAvailableDrone();

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    class AvailableDroneModel {
        private String serialNumber;
        private String model;
        private int weightLimit;
        private int batteryCapacity;
        private String state;
    }

    @GetMapping(value = "drones/{serialNumber}/battery", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    int getDronesBatteryLevel(
            @PathVariable String serialNumber
    );

    @GetMapping(value = "drones/{serialNumber}/load", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    Collection<DronesLoadModel> getDronesLoad(
            @PathVariable String serialNumber
    );

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    class DronesLoadModel {
        private String name;
        private String code;
        private int weight;
        private String image;
    }
}
