package musala.infrastructure.web;

import lombok.AllArgsConstructor;
import musala.domain.commands.LoadDrone;
import musala.domain.commands.RegisterDrone;
import musala.domain.drone.Drone;
import musala.domain.load.Medication;
import musala.domain.queries.DroneBatteryLevelQuery;
import musala.domain.queries.DroneLoadQuery;
import musala.domain.queries.DronesForLoadingQuery;

import java.util.Collection;
import java.util.stream.Collectors;

@AllArgsConstructor
public class DroneControllerImpl implements DroneController {
    private final RegisterDrone registerDrone;
    private final LoadDrone loadDrone;
    private final DroneBatteryLevelQuery droneBatteryLevelQuery;
    private final DronesForLoadingQuery dronesForLoadingQuery;
    private final DroneLoadQuery droneLoadQuery;
    @Override
    public void registerDrone(RegisterDroneRequest registerRequest) {
        registerDrone.execute(
                RegisterDrone.RegisterDroneModel.builder()
                        .serialNumber(registerRequest.getSerialNumber())
                        .model(registerRequest.getModel())
                        .weightLimit(registerRequest.getWeightLimit())
                        .batteryCapacity(registerRequest.getBatteryCapacity())
                        .build()
        );
    }

    @Override
    public void addLoadToDrone(String serialNumber,
                               AddLoadToDroneRequest request) {
        loadDrone.execute(
                new LoadDrone.LoadDroneModel(
                        serialNumber,
                        request.getLoad().stream()
                                .map(loadDetails -> new LoadDrone.LoadModel(
                                        loadDetails.getName(),
                                        loadDetails.getWeight(),
                                        loadDetails.getCode(),
                                        loadDetails.getImage()
                                ))
                                .collect(Collectors.toList())
                        )
        );
    }

    @Override
    public Collection<AvailableDroneModel> getAvailableDrone() {
        // implement additional filters
        return dronesForLoadingQuery.findAvailableForLoadingDrones(null)
                .stream()
                .map(this::mapToModel)
                .collect(Collectors.toList());
    }

    private AvailableDroneModel mapToModel(Drone drone) {
        return new AvailableDroneModel(
                drone.getSerialNumber(),
                drone.getModel().name(),
                drone.getWeightLimit(),
                drone.getBatteryCapacity(),
                drone.getState().name()
        );
    }

    @Override
    public int getDronesBatteryLevel(String serialNumber) {
        return droneBatteryLevelQuery.findBatteryCapacityBySerialNumber(serialNumber);
    }

    @Override
    public Collection<DronesLoadModel> getDronesLoad(String serialNumber) {
        return droneLoadQuery.findLoadByDroneSerialNumber(serialNumber)
                .stream()
                .map(this::mapToModel)
                .collect(Collectors.toList());
    }

    private DronesLoadModel mapToModel(Medication medication) {
        return new DronesLoadModel(
                medication.getName(),
                medication.getCode(),
                medication.getWeight(),
                medication.getImage()
        );
    }
}
